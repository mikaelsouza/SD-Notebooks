import requests
import json
import ast
import math

with open('/Users/mikael/Dev/SD-Notebooks/Data/metadata_scores.json') as file:
    for line in file:
        data = ast.literal_eval(line)

        try:

            json = {
                "name": data['title'],
                "image_uri": data['imUrl'],
                "price": data['price'],
                "brand": "XD",
                "description": data['description'],
                "category": data['categories'][0][-1],
                "asin": data['asin'],
                "score": round(float(data['score']), 5),
            }

        except:
            continue

        r = requests.post(
            "http://127.0.0.1:8000/product/",
            data=json,
        )