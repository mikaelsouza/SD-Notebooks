import requests
import json
import ast
import math

with open('/Users/mikael/Downloads/asin_keywords.json') as file:
    data = json.load(file)

products = 0
total = len(data.items())
for asin, keywords in data.items():
    products += 1
    x = 0
    for keyword in keywords[:5]:
        x += 1
        d = {
            "text": keyword,
            "relevance": x,
            "product": asin,
        }

        r = requests.post(
            "http://127.0.0.1:8000/keyword/",
            data=d,
        )

    if products % 10000 == 0:
        print("Products {} of {}".format(products, total))