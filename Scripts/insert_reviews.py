import requests
import json
import ast
import math

with open('/Users/mikael/Downloads/Amazon/reviews_Electronics.json') as file:
    for line in file:
        data = ast.literal_eval(line)
        try:

            json = {
                "product": data['asin'],
                "text": data['reviewText'],
                "helpfulness": data['helpful'][0],
            }

        except:
            continue

        r = requests.post(
            "http://127.0.0.1:8000/review/",
            data=json,
        )