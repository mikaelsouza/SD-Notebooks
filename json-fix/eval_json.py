import ast
import json
import sys


with open(sys.argv[1]) as f:
    data = f.readlines()

text = list()

total = 0
ok = 0
failed = 0

for line in data:
    try:
        text.append(ast.literal_eval(line))
        ok += 1
    except:
        failed += 1
    
    total += 1

with open(sys.argv[1][:-5] + 'fixed.json', 'w') as f:
    json.dump(text, f)

print("Total: {}".format(total))
print("OK: {}".format(ok))
print("Failed {}".format(failed))